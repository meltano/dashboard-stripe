from setuptools import setup, find_packages

setup(
    name="dashboard-stripe",
    version="0.7",
    description="Meltano reports and dashboards for data fetched using the Stripe API",
    packages=find_packages(),
    package_data={
        "reports": ["*.m5o"],
        "dashboards": ["*.m5o"],
        "snapshots": ["**/*.m5o"],
    },
    install_requires=[],
)
